"""stock_analyzer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from rest_framework import routers
from equities.views import index, StockTickerReportViewSet, user_api_keys, UserAPIAuth
from wifi.views import NetgateViewSet
from django.views.generic import TemplateView
from django.contrib.auth.views import login, logout
from rest_framework_swagger.views import get_swagger_view


admin.autodiscover()

schema_view = get_swagger_view(title='HomeApps API')

equities_router = routers.DefaultRouter()
equities_router.register(r'stock_analyzer', StockTickerReportViewSet, 'stock_analyzer')

wifi_router = routers.DefaultRouter()
wifi_router.register(r'netgate', NetgateViewSet, 'netgate')

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', index),
    url(r'^equities/analysis/', TemplateView.as_view(template_name="equities/analysis.html")),
    # url(r'^wifi/voucher/', login_required(TemplateView.as_view(template_name="wifi/get_voucher.html")), name="voucher"),
    url(r'^wifi/voucher/', TemplateView.as_view(template_name="wifi/get_voucher.html"), name="voucher"),
    url(r'^accounts/login/', login, {'template_name': 'auth/login.html'}, name="login"),
    url(r'^accounts/logout/$', logout, name="account_logout"),
    url(r'^user/apikeys/$', user_api_keys),
    url(r'^api/apikey(|/)$',UserAPIAuth.as_view()),
    url(r'^equities/api/v1/', include(equities_router.urls)),
    url(r'^swagger', schema_view),
    url(r'^wifi/api/v1/', include(wifi_router.urls)),
    url(r'^health/', include('health_check.urls')),
]
