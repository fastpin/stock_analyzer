/*
 * This file contains common non-object directed funstions. Such as padLeft and
 * formatDate.
 *
 * Author: Matthew Stroud
 * email: mattstroud@overstock.com
 *
 * 2016/19/04 - mattstroud - Created
 */



function padLeft(nr, n, str){
    /* This pads the string with zeros on the left side.
     *
     * nr:  The item you wish to pad
     * n:   Required length
     * str: What you wish to pad with
     */
    return Array(n-String(nr).length+1).join(str||'0')+nr;
}

function formatUnixTime(timestamp) {
    /* This formats a unix timestamp in to 'YYYY/MM/DD HH:MM:SS'
     *
     * timestamp:   The Unix timestamp you wish to reformat
     *
     */
    var tmpDate = new Date(timestamp * 1000);
    var strDate = tmpDate.getFullYear() + '/'
                + padLeft((tmpDate.getMonth() + 1 ).toString(),2) + '/'
                + padLeft(tmpDate.getDate().toString(),2) + ' '
                + padLeft(tmpDate.getHours(),2) + ':'
                + padLeft(tmpDate.getMinutes(),2) + ':'
                + padLeft(tmpDate.getSeconds(),2);
    return strDate;
}

