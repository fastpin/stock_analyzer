$(function(){
    $.ajax({
        url: '/api/apikey/',
        type: "GET",
        contentType: 'application/json',
        success: function(data, textStatus, jqXHR) {
            $('#keyElem').append(data['Token']);
        },
        error: function() {
            $('#keyElem').append('Oh no! Something went wrong. We are trying to fix it.');
        },
    });

});