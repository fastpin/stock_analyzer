$(function () {
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $('.TickerInput').keyup(function(event) {
        if (event.keyCode === 13) {
            $('.CheckTickerBtn').click();
        }
    });

    $('.TickerDaysInput').keyup(function(event) {
        if (event.keyCode === 13) {
            $('.CheckTickerBtn').click();
        }
    });

    $('body').delegate('.CheckTickerBtn', 'click', function () {
        $('.equities_alerts').removeClass('alert-success');
        $('.equities_alerts').hide()
        ticker = $('.TickerInput').val();
        days = $('.TickerDaysInput').val();
            $.ajax({
                url: "/equities/api/v1/stock_analyzer/",
                type: "POST",
                data: JSON.stringify({
                    ticker_name: ticker,
                    days: days
                }),
                contentType: 'application/json',
                success: function (data) {
                    $.get("/static/mustache/templates/equities/analysis_result_table.html?ver=" + Math.random(), function (template) {
                        // $('.equities_analyzer').append(Mustache.render(template, {data: data}));
                        $('#myTable > tbody:last-child').append(Mustache.render(template, {data: data}));
                        // console.log(data)
                        $('.equities_alerts').html("Success...")
                        $('.equities_alerts').removeClass('alert-danger');
                        $('.equities_alerts').addClass('alert-success');
                        $('.equities_alerts').show();
                        console.log(data)
                    })

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    console.log(xhr);
                    $('.equities_alerts').html("Oh no! Something didn't work....<br>" +
                    "Server response was " + xhr.responseText + " ")
                    $('.equities_alerts').removeClass('alert-success');
                    $('.equities_alerts').addClass('alert-danger');
                    $('.equities_alerts').show();
                }
            });
    });

    $('body').delegate('.refresh', 'click', function () {
            location.reload();
        }
    );
});