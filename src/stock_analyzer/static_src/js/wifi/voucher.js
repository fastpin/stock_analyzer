$(function () {
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    function GetVoucher(){
        $.ajax({
            url: '/wifi/api/v1/netgate/1/next_voucher/',
            type: "GET",
            contentType: 'application/json',
            success: function (data) {
                $.get('/static/mustache/templates/wifi/voucher_result.html?vers=' + Math.random(), function(template) {
                    $('#voucher_data').html(Mustache.render(template, {data: data}));
                })
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                $('#voucher_data').html("Oops, something went wrong getting your voucher.<br>" + xhr.status + " - " + thrownError);
                $('#voucher_data').addClass('alert-danger');
                $('#voucher_data').show();
            }
        });
    }
    GetVoucher();
});