from rest_framework import serializers
from wifi.models import VoucherRoll, CaptivePortalZone, NetgateInstance

class NetgateSerializer(serializers.ModelSerializer):

    class Meta:
        model = NetgateInstance
        fields = ('id', 'public_dns')

class NextVoucherSerializer(serializers.Serializer):
    minutes = serializers.CharField(required=True)
    count = serializers.CharField(required=True)
    used = serializers.CharField(required=True)
    comment = serializers.CharField(allow_blank=True)
    vouchers = serializers.ListField()
    active = serializers.JSONField(required=False)

    def validate(self, data):
        if int(data['used']) < len(data['vouchers']):
            if int(data['minutes']) > 2:
                return data
            else:
                raise serializers.ValidationError("All vouchers have less than one minute validity.")
        else:
            raise serializers.ValidationError("No more available vouchers found")