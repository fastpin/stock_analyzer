from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
import requests
from rest_framework.permissions import IsAuthenticated
from rest_framework import status, viewsets, mixins, exceptions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.decorators import detail_route
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from wifi.serializers import NetgateSerializer, NextVoucherSerializer
from wifi.models import VoucherRoll, CaptivePortalZone, NetgateInstance
from wifi.lib.netgate import Netgate
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer

# Create your views here.


class NetgateViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    serializer_class = NetgateSerializer
    queryset = NetgateInstance.objects.filter(is_primary=True)
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    @detail_route(methods=['GET',], permission_classes=[IsAuthenticated,])
    def vouchers(self, request, pk):
        ng_inst = self.get_object()
        ng = Netgate(ng_inst.address, port=ng_inst.ssl_port)
        zone = CaptivePortalZone.objects.get(NetgateInstance=ng_inst)
        rolls = VoucherRoll.objects.filter(CaptivePortalZone=zone)
        if len(rolls) == 1:
            vc_res = ng.get_vouchers(zone.name, rolls[0].roll_id, ng_inst.cp_auth)
            return Response(data=vc_res, status=status.HTTP_200_OK)
        else:
            return Response(data={"Error": "Invalid roll count found for zone"}, status=status.HTTP_400_BAD_REQUEST)

    @detail_route(methods=['GET',], serializer_class=NextVoucherSerializer)
    def next_voucher(self, request, pk):
        ng_inst = self.get_object()
        ng = Netgate(ng_inst.address, port=ng_inst.ssl_port)
        zone = CaptivePortalZone.objects.get(NetgateInstance=ng_inst)
        rolls = VoucherRoll.objects.filter(CaptivePortalZone=zone)
        if len(rolls) == 1:
            vc_res = ng.get_vouchers(zone.name, rolls[0].roll_id, ng_inst.cp_auth)
            nv_ser = NextVoucherSerializer(data=vc_res)
            nv_ser.is_valid(raise_exception=True)
            cp_url = "https://{}:{}/index.php?zone={}".format(ng_inst.public_dns, ng_inst.cp_port, zone.name)
            active_vouchers = nv_ser.validated_data.get('active', [])
            if active_vouchers:
                active = []
                for voucher in active_vouchers:
                    active.append(active_vouchers[voucher]['active_voucher'])
                active_vouchers = active
            return Response(data={"voucher": nv_ser.validated_data['vouchers'][int(nv_ser.validated_data['used'])],
                                  "portal_url": cp_url,
                                  "minutes": nv_ser.validated_data['minutes'],
                                  "active": active_vouchers}, status=status.HTTP_200_OK)
        else:
            return Response(data={"Error": "Invalid roll count found for zone"}, status=status.HTTP_400_BAD_REQUEST)
