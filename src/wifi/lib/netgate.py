import json
import requests


class Netgate(object):
    def __init__(self, ip_addr, **kwargs):
        self.ip_addr = ip_addr
        self.ssl = kwargs.get('ssl', True)
        self.port = kwargs.get('port', 443)

        if self.ssl:
            self.baseurl = "https://{}:{}/".format(self.ip_addr, self.port)
        else:
            self.baseurl = "http://{}:{}/".format(self.ip_addr, self.port)

    def __get_request(self, url, **kwargs):

        if 'params' in kwargs.keys():
            params = kwargs['params']
        else:
            params = None
        if 'headers' in kwargs.keys():
            headers = kwargs['headers']
        else:
            headers = None

        full_url = self.baseurl + url
        res = requests.get(full_url, headers=headers, verify=False, timeout=10.0, params=params)

        if res.status_code not in range(200, 300):
            res.raise_for_status()


        parsed = json.loads(res.content.decode('latin1'))
        return parsed

    def get_vouchers(self, zone, roll, auth):
        url = "cp.php"

        i_params = {"zone": "{}:{}".format(zone, roll)}
        i_headers = {"AUTH": auth}
        r = self.__get_request(url, params=i_params, headers=i_headers)

        return r