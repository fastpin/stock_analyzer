from django.contrib import admin
from wifi.models import CaptivePortalZone, VoucherRoll, NetgateInstance
from import_export.admin import ImportExportModelAdmin
from import_export import resources


# Register your models here.
class CaptivePortalZoneResource(resources.ModelResource):
    class Meta:
        model = CaptivePortalZone


class CaptivePortalZoneModel(ImportExportModelAdmin):
    resource_class = CaptivePortalZoneResource


class NetgateInstanceResource(resources.ModelResource):
    class Meta:
        model = NetgateInstance


class NetgateInstanceModel(ImportExportModelAdmin):
    resource_class = NetgateInstanceResource


admin.site.register(CaptivePortalZone, CaptivePortalZoneModel)
admin.site.register(NetgateInstance, NetgateInstanceModel)
admin.site.register(VoucherRoll)