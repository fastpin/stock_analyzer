from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class NetgateInstance(models.Model):
    address = models.GenericIPAddressField(verbose_name="Primary IP address", blank=False, null=False)
    ssl_port = models.IntegerField(default=443, blank=False, null=True, help_text="The port for the pfSense WebUI")
    name = models.CharField(max_length=255, blank=True, null=True, help_text="Friendly name or Internal DNS")
    public_dns = models.CharField(max_length=255, blank=True, null=True, help_text="The public DNS name")
    cp_auth = models.CharField(max_length=255, blank=True, null=True, help_text="Secret used to retrieve CP vouchers")
    cp_port = models.IntegerField(default=8003, blank=True, null=True, help_text="The port for the CP portal WebUI")
    is_primary = models.BooleanField(default=True, help_text="Is this the primary NetGate instance?")
    is_virtual = models.BooleanField(default=False, help_text="Is this a virtual NetGate instance")

    def __str__(self):
        return "{}".format(self.address)


class CaptivePortalZone(models.Model):
    name = models.CharField(null=False, blank=False, max_length=80, unique=True, help_text="The pfSense Captive Portal Zone Name")
    NetgateInstance = models.ForeignKey(NetgateInstance, related_name='netgate')

    def __str__(self):
        return "Zone: {}".format(self.name)


class VoucherRoll(models.Model):
    roll_id = models.IntegerField(blank=False, null=False, default=0, help_text="Voucher Rolls defined for CP Zone")
    CaptivePortalZone = models.ForeignKey(CaptivePortalZone, related_name='zone')

    def __str__(self):
        return "Roll: {} (Zone: {})".format(self.roll_id, self.CaptivePortalZone.name)