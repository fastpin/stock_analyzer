from rest_framework.test import APITestCase
from rest_framework import status

class TestEquitiesViews(APITestCase):
    fixtures = ['init.json']

    def test_Default_data_EquitiesAnalyze(self):
        response = self.client.get('/equities/api/v1/stock_analyzer/')
        self.assertEqual(response.status_code, status.HTTP_200_OK, response)
        self.assertEqual(int(60), response.data[0]['default_days'])

    def test_gather_equities_data(self):
        response = self.client.post('/equities/api/v1/stock_analyzer/', data={"ticker_name": "BBY", "days": 50})
        self.assertEqual(int(50), response.data['days'], msg="Invalid days returned %s" % response.data)
        print(response.data)
        self.assertIsInstance(response.data['analysis']['day_avg_return'], float,
                              msg="Average Daily Return should be a float")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
