from django.shortcuts import render
from django.contrib.auth.decorators import login_required, user_passes_test
from rest_framework.views import APIView
from rest_framework import status, viewsets, mixins, exceptions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from django.contrib.auth import authenticate
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from equities.serializers import StockTickerReportSerializer, StockTickerListSerializer
from equities.models import EquitiesTracker
from rest_framework.authtoken.models import Token
from equities.models import EquitiesTracker

# Create your views here.

#@login_required
def index(request):
    return render(request, 'index.html')

@login_required
def user_api_keys(request):
    return render(request, 'user/apikeys.html')

class StockTickerReportViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                               mixins.UpdateModelMixin, viewsets.GenericViewSet):
    serializer_class = StockTickerListSerializer

    def get_queryset(self):
        queryset = EquitiesTracker.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        if 'days' in request.data.keys():
            if not request.data['days']:
                try:
                    db_days = EquitiesTracker.objects.get()
                    request.data['days'] = db_days.default_days
                except EquitiesTracker.DoesNotExist:
                    request.data['days'] = 60
                except Exception as e:
                    raise Exception("Some internal issue happened setting default days: %s" % e.args)
        ser = StockTickerReportSerializer(data=request.data)
        ser.is_valid(raise_exception=True)

        return Response(ser.data, status=status.HTTP_200_OK)

class UserAPIAuth(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication)

    def get(self, request, format=None):

        if request.user.is_authenticated() is True:

            token, created = Token.objects.get_or_create(user=request.user)

            content = {
                'user': str(request.user),
                'Token': str(token.key),
                'tokenisnew': str(created),
            }
            return Response(content, status=status.HTTP_200_OK)
        else:
            raise exceptions.AuthenticationFailed('Authentication failure')

    def post(self, request, format=None):
        data = request.data
        user = data['username']
        password = data['password']
        user_obj = authenticate(username=user, password=password)
        if user_obj is not None:
            token, created = Token.objects.get_or_create(user=user_obj)

            content = {
                'user': str(user_obj.username),
                'Token': str(token.key),
                'tokenisnew': str(created),
            }
            return Response(content, status=status.HTTP_200_OK)
        else:
            raise exceptions.AuthenticationFailed('Authentication failure')