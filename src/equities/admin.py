from django.contrib import admin
from equities.models import EquitiesTracker
from import_export.admin import ImportExportModelAdmin
from import_export import resources
# Register your models here.
class EquitiesTrackerResource(resources.ModelResource):
    class Meta:
        model = EquitiesTracker

class EquitiesTrackerModel(ImportExportModelAdmin):
    resource_class = EquitiesTrackerResource
    pass

admin.site.register(EquitiesTracker, EquitiesTrackerModel)