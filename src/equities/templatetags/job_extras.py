import re
from django import template
from django.contrib.auth.models import Group
from django.conf import settings
import logging

logger = logging.getLogger('template')
register = template.Library()


@register.filter(name='job_id_to_str')
def job_id_to_str(job):
    return str(job['_id'])

@register.filter(name='set_form_class')
def set_form_class(field, css):
    return field.as_widget(attrs={"class": css})

@register.filter(name='sizeof_format')
def sizeof_format(bytes, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(bytes) < 1024.0:
            return "%3.1f %s%s" % (bytes, unit, suffix)
        bytes /= 1024.0
    return "%.1f %s%s" % (bytes, 'Yi', suffix)

@register.filter(name='make_jira_link')
def make_jira_link(issue):
    url = settings.JIRA['server'] + '/browse/' + issue
    return url

@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    return group in user.groups.all()

@register.filter(name='process_policy_list', is_safe=True)
def process_policy_list(polices, length=20):
    """
    This function processes the list of policies that a client might have
    :param polices: list of BackupPolicy objects
    :type polices: ``list``
    :return: html to implemented in a template
    :rtype: ``str``
    """
    pol_names = list()
    for policy in polices:
        pol_names.append(policy.name)
    msg = ', '.join(pol_names)
    if len(msg) > length:
        msg = msg[:length]
        msg += '...'
    popover = str()
    for policy in polices:
        re_policy = re.search('(?P<hour>\d+)\_(?P<type>(STD|WIN))_FS_(?P<site>(C|SE))', policy.name, re.I)
        if re_policy is not None:
            elems = re_policy.groupdict()
            if int(elems['hour']) > 12:
                hour = str(int(elems['hour']) - 12)
                meridiem = 'PM'
            else:
                hour = elems['hour']
                meridiem = 'AM'
            if elems['type'] == 'STD':
                htype = 'Linux'
            else:
                htype = 'Windows'
            if elems['site'] == 'C':
                site = 'Old Mill I'
            else:
                site = 'Single Edge'
            popover += " %s: %s Filesystem Backup at %s starting at %s%s." % (policy.name, htype, site, hour, meridiem)
        else:
            popover += " %s: This is custom policy. Please contact the Backups team for more information." % (policy.name)
    ret = "<button type=\"button\" class=\"btn btn-default btn-small\" data-toggle=\"popover\" title=\"Policy(s)\" data-content=\"" + popover + "\">" + msg + "</button>"
    return ret

@register.filter(name="disable_backups_remove")
def disable_backups_remove(client):
    for policy in client.Policy.all():
        if ('crit' in policy.name.lower() or
                    'ndmp' in policy.name.lower() or
                    'nfs' in policy.name.lower() or
                    'exchange' in policy.name.lower() or
                    'sharepoint' in policy.name.lower() or
                    '_ad_' in policy.name.lower() or
                    'BACK-' in policy.name.lower()):
            return 'disabled="disabled"'
    if ('bkmedia' in client.name or
                'tdbar' in client.name):
        return 'disabled="disabled"'
    else:
        return str()

@register.filter(name="page_ranger")
def page_range(num):
    return range(1,int(num) + 1)

@register.assignment_tag
def get_help_link():
    logger.info(settings.HELP['confluence_tapes'])
    return settings.HELP['confluence_tapes']


@register.simple_tag
def get_setting(name):
    return getattr(settings, name, "")
