from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class EquitiesTracker(models.Model):
    default_days = models.IntegerField(help_text="Default number of days used for gathering stock data",
                                       default=60,
                                       null=False,
                                       blank=False)
    submitted = models.DateTimeField(auto_now_add=True, editable=False, null=True)
    # user = models.ForeignKey(User)

    def __str__(self):
        return "Default days: %i" % self.default_days
