#!/usr/bin/env python

"""
get-yahoo-quotes.py:  Script to download Yahoo historical quotes using the new cookie authenticated site.
"""

import re
import sys
import datetime
import requests


class YahooTicker(object):
    def __init__(self, ticker, **kwargs):
        self.symbol = ticker
        self.make_csv = kwargs.get('make_csv', False)
        self.days = kwargs.get('days', 45)
        d_adj = datetime.datetime.today().isoweekday() + self.days
        default_start = int((datetime.datetime.today() + datetime.timedelta(days=-d_adj)).timestamp())
        self.start_date = kwargs.get('start_date', default_start)
        default_end = int(datetime.datetime.today().timestamp())
        self.end_date = kwargs.get('end_date', default_end)

    def split_crumb_store(self, v):
        return v.split(':')[2].strip('"')

    def find_crumb_store(self, lines):
        # Looking for
        # ,"CrumbStore":{"crumb":"9q.A4D1c.b9
        for l in lines:
            if re.findall(r'CrumbStore', l):
                return l
        print("Did not find CrumbStore")

    def get_cookie_value(self, r):
        return {'B': r.cookies['B']}

    def get_page_data(self, symbol):
        url = "https://finance.yahoo.com/quote/%s/?p=%s" % (symbol, symbol)
        r = requests.get(url)
        cookie = self.get_cookie_value(r)

        # Code to replace possible \u002F value
        # ,"CrumbStore":{"crumb":"FWP\u002F5EFll3U"
        # FWP\u002F5EFll3U
        lines = r.content.decode('unicode-escape').strip(). replace('}', '\n')
        return cookie, lines.split('\n')

    def get_cookie_crumb(self, symbol):
        cookie, lines = self.get_page_data(symbol)
        crumb = self.split_crumb_store(self.find_crumb_store(lines))
        return cookie, crumb

    def get_data(self, symbol, start_date, end_date, cookie, crumb, **kwargs):
        make_csv = kwargs.get('make_csv', False)
        url = "https://query1.finance.yahoo.com/v7/finance/download/%s?period1=%s&period2=%s&interval=1d&events=history&crumb=%s" % (symbol, start_date, end_date, crumb)
        response = requests.get(url, cookies=cookie)

        if make_csv:
            filename = '%s.csv' % (symbol)
            with open (filename, 'wb') as handle:
                for block in response.iter_content(1024):
                    handle.write(block)

        return response.text

    def download_quotes(self):
        start_date = 0
        end_date = int(datetime.datetime.now().timestamp())
        cookie, crumb = self.get_cookie_crumb(self.symbol)
        result = self.get_data(self.symbol, self.start_date, self.end_date, cookie, crumb, make_csv=self.make_csv)
        return result

class YahooTickerExch(object):
    def __init__(self, ticker, **kwargs):
        self.symbol = ticker

    def get_ticker_name(self):
        url = "http://d.yimg.com/aq/autoc?query=%s&region=US&lang=en-US&format=json" % self.symbol
        response = requests.get(url)
        out = response.json()

        for exch in out['ResultSet']['Result']:
            re_ticker = re.compile(r"^\s*%s\s*" % self.symbol, re.IGNORECASE)
            if re_ticker.match(exch['symbol']):
                return exch['name']

        return "<Unknown name>"

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print("\nUsage: get-yahoo-quotes.py SYMBOL\n\n")
    else:
        for i in range(1, len(sys.argv)):
            symbol = sys.argv[i]
            ticker_obj = YahooTicker(symbol, make_csv=True)
            print("--------------------------------------------------")
            print("Downloading %s to %s.csv" % (symbol, symbol))
            ticker_obj.download_quotes()
            print("--------------------------------------------------")