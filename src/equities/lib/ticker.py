import datetime
import numpy as np
import pandas as pd
import json
from math import sqrt
from pandas_finance import Equity
import pandas_datareader.data as web
import urllib
from pprint import pprint
import re
import statistics
from equities.lib.get_yahoo_quotes import YahooTicker, YahooTickerExch

# def get_historic_data(ticker,
#                       start_date=datetime.datetime(2018, 1, 15, 00, 00),
#                       end_date=datetime.date.today()):
#     """
#     Obtains data from Google Finance and adds it to a pandas DataFrame object.
#
#     ticker: Yahoo Finance ticker symbol, e.g. "GOOG" for Google, Inc.
#     start_date: Start date in (YYYY, M, D) format
#     end_date: End date in (YYYY, M, D) format
#     """
#
#     # Construct the Yahoo URL with the correct integer query parameters
#     # for start and end dates. Note that some parameters are zero-based!
#     # yahoo_url = "http://ichart.finance.yahoo.com/table.csv?s=%s&a=%s&b=%s&c=%s&d=%s&e=%s&f=%s" % (ticker, start_date[1] - 1, start_date[2], start_date[0], end_date[1] - 1, end_date[2], end_date[0])
#     # yahoo_url = "https://query1.finance.yahoo.com/v7/finance/download/GE?period1=1516302151&period2=1518980551&interval=1d&events=history&crumb=NPpVyJr0O2r"
#     google_url = 'https://finance.google.com/finance/historical?q=%s&startdate=%s&enddate=%s&output=csv' % \
#                  (ticker, start_date.strftime('%Y/%m/%d'), end_date.strftime('%Y/%m/%d'))
#     print(google_url)
#
#     # Try connecting to Google Finance and obtaining the data
#     # On failure, print an error message
#     try:
#         fin_data = urllib.request.urlopen(google_url)
#     except urllib.error.URLError:
#         raise Exception("The URL was invalid.")
#     # except urllib.Exceptions.gaierror as e:
#     #     print("Could not download Yahoo data: %s" % e)
#
#     # Create the (temporary) Python data structures to store
#     # the historical data
#     date_list = []
#     hist_data = [[] for i in range(6)]
#
#     # Format and copy the raw text data into datetime objects
#     # and floating point values (still in native Python lists)
#     response = fin_data.read().decode('utf-8')
#     list = response.split('\n')
#     # pprint(list)
#
#     new_list = []
#     for elt in list[1:-1]:
#         series = elt.split(',')
#         inbound = series
#         inbound.append(series[4])
#         new_list.append(','.join(inbound))
#
#     # pprint(new_list)
#
#     for day in new_list:  # Avoid the header line in the CSV
#         headers = day.rstrip().split(',')
#         date_list.append(datetime.datetime.strptime(headers[0], '%d-%b-%y'))
#         for i, header in enumerate(headers[1:]):
#             hist_data[i].append(float(header))
#
#     # Create a Python dictionary of the lists and then use that to
#     # form a sorted Pandas DataFrame of the historical data
#     hist_data = dict(zip(['open', 'high', 'low', 'close', 'volume', 'adj_close'], hist_data))
#     pdf = pd.DataFrame(hist_data, index=pd.Index(date_list))
#
#     return pdf

def standard_deviation(lst, population=True, **kwargs):
    """Calculates the standard deviation for a list of numbers."""
    verbose = kwargs.get('verbose', False)
    num_items = len(lst)
    mean = sum(lst) / num_items
    differences = [x - mean for x in lst]
    sq_differences = [d ** 2 for d in differences]
    ssd = sum(sq_differences)

    # Note: it would be better to return a value and then print it outside
    # the function, but this is just a quick way to print out the values along
    # the way.
    if population is True:
        if verbose:
            print('This is POPULATION standard deviation.')
        variance = ssd / num_items
    else:
        if verbose:
            print('This is SAMPLE standard deviation.')
        variance = ssd / (num_items - 1)
    sd = sqrt(variance)
    # You could `return sd` here.
    if verbose:
        print('The mean of {} is {}.'.format(lst, mean))
        print('The differences are {}.'.format(differences))
        print('The sum of squared differences is {}.'.format(ssd))
        print('The variance is {}.'.format(variance))
        print('The standard deviation is {}.'.format(sd))
        print('--------------------------')
    res = {'std_dev': sd, 'var': variance}
    return res


def get_historic_data(ticker,
                      start_date=(2018, 1, 18),
                      end_date=datetime.date.today().timetuple()[0:3],
                      **kwargs):
    """
    Obtains data from Yahoo Finance and adds it to a pandas DataFrame object.

    ticker: Yahoo Finance ticker symbol, e.g. "GOOG" for Google, Inc.
    start_date: Start date in (YYYY, M, D) format
    end_date: End date in (YYYY, M, D) format
    """
    days = kwargs.get('days', 45)
    csv = kwargs.get('csv', False)
    # Construct the Yahoo URL with the correct integer query parameters
    # for start and end dates. Note that some parameters are zero-based!
    s_y, s_m, s_d = start_date
    start = int(datetime.datetime(s_y, s_m, s_d).timestamp())
    e_y, e_m, e_d = end_date
    end = int(datetime.datetime(e_y, e_m, e_d).timestamp())


    # Try connecting to Yahoo Finance and obtaining the data
    # On failure, print an error message

    yh_ticker = YahooTicker(ticker, days=days, make_csv=csv)
    try:
        yf_data = yh_ticker.download_quotes().split('\n')[:-1]
    except urllib.error.URLError:
        raise Exception("The URL was invalid.")
    except Exception:
        raise Exception("Unknown error")

    # Create the (temporary) Python data structures to store
    # the historical data
    date_list = []
    hist_data = [[] for i in range(6)]

    # pprint(yf_data)

    # Format and copy the raw text data into datetime objects
    # and floating point values (still in native Python lists)
    for day in yf_data[1:]:  # Avoid the header line in the CSV
        if re.match('(?i)(?:.*?null)', day):
            continue
        headers = day.rstrip().split(',')
        date_list.append(datetime.datetime.strptime(headers[0], '%Y-%m-%d'))
        for i, header in enumerate(headers[1:]):
            hist_data[i].append(float(header))

    # Create a Python dictionary of the lists and then use that to
    # form a sorted Pandas DataFrame of the historical data
    hist_data = dict(zip(['open', 'high', 'low', 'close', 'adj_close', 'volume'], hist_data))
    pdf = pd.DataFrame(hist_data, index=pd.Index(date_list))

    # open = pdf['open'].resample('W-MON').last()
    # close = pdf['close'].resample('W-FRI').last().resample('W-MON').last()
    # adj_close = pdf['adj_close'].resample('W-FRI').last().resample('W-MON').last()
    # high = pdf['high'].resample('W-MON').max()
    # low = pdf['low'].resample('W-MON').min()
    # vol = pdf['volume'].resample('W-MON').sum()
    # weekly_data = pd.concat([open, close, adj_close, high, low, vol], axis=1)
    # pprint(weekly_data)
    # weekly_data['weekly_ret'] = weekly_data['adj_close'].pct_change(fill_method=None).dropna()
    # pprint(weekly_data['weekly_ret'])
    #
    # pdf['daily_ret'] = pdf['adj_close'].pct_change(fill_method=None).dropna()
    # pprint(pdf['daily_ret'])

    return pdf

def annualised_sharpe(returns, N=252):
    """
    Calculate the annualised Sharpe ratio of a returns stream
    based on a number of trading periods, N. N defaults to 252,
    which then assumes a stream of daily returns.

    The function assumes that the returns are the excess of
    those compared to a benchmark.
    """
    return np.sqrt(N) * returns.mean() / returns.std()


def equity_sharpe(ticker):
    """
    Calculates the annualised Sharpe ratio based on the daily
    returns of an equity ticker symbol listed in Yahoo Finance.

    """

    # Obtain the equities daily historic data for the desired time period
    # and add to a pandas DataFrame
    # pdf = get_historic_data(ticker, start_date=(2000, 1, 1), end_date=(2013, 5, 29))
    pdf = get_historic_data(ticker)
    # Use the percentage change method to easily calculate daily returns
    pdf['daily_ret'] = pdf['adj_close'].pct_change(fill_method=None).dropna()

    # Assume an average annual risk-free rate over the period of 5%
    pdf['excess_daily_ret'] = pdf['daily_ret'] - 0.05 / 252

    # Return the annualised Sharpe ratio based on the excess daily returns
    return annualised_sharpe(pdf['excess_daily_ret'])


def market_neutral_sharpe(ticker, benchmark):
    """
    Calculates the annualised Sharpe ratio of a market
    neutral long/short strategy inolving the long of 'ticker'
    with a corresponding short of the 'benchmark'.
    """

    # Get historic data for both a symbol/ticker and a benchmark ticker
    # The dates have been hardcoded, but you can modify them as you see fit!
    tick = get_historic_data(ticker)
    bench = get_historic_data(benchmark)
    # Calculate the percentage returns on each of the time series
    tick['daily_ret'] = tick['adj_close'].pct_change(fill_method=None).dropna()
    bench['daily_ret'] = bench['adj_close'].pct_change(fill_method=None).dropna()

    # Create a new DataFrame to store the strategy information
    # The net returns are (long - short)/2, since there is twice
    # trading capital for this strategy
    strat = pd.DataFrame(index=tick.index)
    strat['net_ret'] = (tick['daily_ret'] - bench['daily_ret']) / 2.0

    # Return the annualised Sharpe ratio for this strategy
    return annualised_sharpe(strat['net_ret'])

def weekly_std_dev(ticker, **kwargs):
    verbose = kwargs.get('verbose', False)
    days = kwargs.get('days', 45)
    csv = kwargs.get('csv', False)
    pdf = get_historic_data(ticker, days=days, csv=csv)
    open = pdf['open'].resample('W-MON').last()
    close = pdf['close'].resample('W-FRI').last().resample('W-MON').last()
    adj_close = pdf['adj_close'].resample('W-FRI').last().resample('W-MON').last()
    high = pdf['high'].resample('W-MON').max()
    low = pdf['low'].resample('W-MON').min()
    vol = pdf['volume'].resample('W-MON').sum()
    weekly_data = pd.concat([open, close, adj_close, high, low, vol], axis=1)
    weekly_data['weekly_ret'] = weekly_data['adj_close'].pct_change(periods=1, fill_method=None).dropna()

    returns = []
    for ind, val in weekly_data['weekly_ret'].items():
        fixed_val = round((val * -1), 6)
        if not re.match('(?i)(?:.*?NaN)', str(val)):
            # print("Weekly Date: %s Daily Returns: %s" % (ind, fixed_val))
            if not datetime.datetime.strptime(ind._short_repr, '%Y-%m-%d') > datetime.datetime.today():
                returns.append(fixed_val)
    # pprint(returns)
    avg_returns = round(np.array(returns).mean(), 6)
    stats_std_dev = standard_deviation(returns, population=True)
    if verbose:
        pprint(weekly_data)
        # pprint(weekly_data['weekly_ret'])
        print("--------------------------------------------------")
        print("Ticker: %s" % ticker)
        print("Average percent is: %f" % float(avg_returns * 100) + "%")
        # std_dev = np.std(returns, ddof=avg_returns)
        # print("Standard Deviation: %s" % std_dev)
        print("Standard Deviation is: %s" % stats_std_dev['std_dev'])
        print("Variance is: %s" % stats_std_dev['var'])
        print("--------------------------------------------------")

    newName = YahooTickerExch(ticker)
    name = newName.get_ticker_name()
    # pprint(name)
    # result = {'avg_daily_return': avg_returns,
    #           'std_dev': round(stats_std_dev['std_dev'], 9),
    #           'variance': round(stats_std_dev['var'], 9),
    #           'ticker': ticker}
    result = {'wk_avg_return': avg_returns,
              'wk_std_dev': round(stats_std_dev['std_dev'], 4),
              'ticker': ticker,
              'name': name}
    return result

def daily_std_dev(ticker, **kwargs):
    verbose = kwargs.get('verbose', False)
    days = kwargs.get('days', 45)
    csv = kwargs.get('csv', False)
    pdf = get_historic_data(ticker, days=days, csv=csv)
    # pdf = get_historic_data(ticker)
    pdf['daily_ret'] = pdf['adj_close'].pct_change(periods=1, fill_method=None).dropna()
    # pprint(pdf['daily_ret'])

    returns = []
    for ind, val in pdf['daily_ret'].items():
        fixed_val = round((val * -1), 6)
        if not re.match('(?i)(?:.*?NaN)', str(val)):
            if verbose:
                print("Daily Date: %s Daily Returns: %s" % (ind, fixed_val))
            returns.append(fixed_val)
        if verbose:
            print("Omitting Date: %s Daily Returns: %s" % (ind, fixed_val))
    avg_returns = round(np.array(returns).mean(), 6)


    # std_dev = np.std(returns, ddof=avg_returns)
    # print("Standard Deviation: %s" % std_dev)
    newName = YahooTickerExch(ticker)
    name = newName.get_ticker_name()

    stats_std_dev = standard_deviation(returns, population=True)
    if verbose:
        print("Average percent is: %f" % float(avg_returns * 100) + "%")
        print("Standard Deviation is: %s" % stats_std_dev['std_dev'])
        print("Variance is: %s" % stats_std_dev['var'])

    result = {'day_avg_return': avg_returns,
              'day_std_dev': round(stats_std_dev['std_dev'], 4),
              'ticker': ticker,
              'name': name}
    return result

def combined_day_week(ticker, **kwargs):
    verbose = kwargs.get('verbose', False)
    days = kwargs.get('days', 45)
    csv = kwargs.get('csv', False)
    pdf = get_historic_data(ticker, days=days, csv=csv)
    pdf['daily_ret'] = pdf['adj_close'].pct_change(periods=1, fill_method=None).dropna()

    returns = []
    for ind, val in pdf['daily_ret'].items():
        fixed_val = round(val, 6)
        if not re.match('(?i)(?:.*?NaN)', str(val)):
            if not datetime.datetime.strptime(ind._short_repr, '%Y-%m-%d') > datetime.datetime.today():
                returns.append(fixed_val)
                if verbose:
                    print("Daily Date: %s Daily Returns: %s" % (ind, fixed_val))
            if verbose:
                print("Omitting Date: %s Daily Returns: %s" % (ind, fixed_val))
    daily_avg_returns = round(np.array(returns).mean(), 6)

    # std_dev = np.std(returns, ddof=avg_returns)
    # print("Standard Deviation: %s" % std_dev)

    daily_stats_std_dev = standard_deviation(returns, population=True)
    if verbose:
        print("Average percent is: %f" % float(daily_avg_returns * 100) + "%")
        print("Standard Deviation is: %s" % daily_stats_std_dev['std_dev'])
        print("Variance is: %s" % daily_stats_std_dev['var'])


    open = pdf['open'].resample('W-MON').last()
    close = pdf['close'].resample('W-FRI').last().resample('W-MON').last()
    adj_close = pdf['adj_close'].resample('W-FRI').last().resample('W-MON').last()
    high = pdf['high'].resample('W-MON').max()
    low = pdf['low'].resample('W-MON').min()
    vol = pdf['volume'].resample('W-MON').sum()
    weekly_data = pd.concat([open, close, adj_close, high, low, vol], axis=1)
    weekly_data['weekly_ret'] = weekly_data['adj_close'].pct_change(periods=1, fill_method=None).dropna()

    wk_returns = []
    for ind, val in weekly_data['weekly_ret'].items():
        fixed_val = round(val, 6)
        if not re.match('(?i)(?:.*?NaN)', str(val)):
            # print("Weekly Date: %s Daily Returns: %s" % (ind, fixed_val))
            if not datetime.datetime.strptime(ind._short_repr, '%Y-%m-%d') > datetime.datetime.today():
                wk_returns.append(fixed_val)
                if verbose:
                    print("Weekly Date: %s Weekly Returns: %s" % (ind, fixed_val))

    weekly_avg_returns = round(np.array(wk_returns).mean(), 6)
    weekly_stats_std_dev = standard_deviation(wk_returns, population=True)
    if verbose:
        pprint(returns)
        # pprint(weekly_data)
        # pprint(weekly_data['weekly_ret'])
        print("--------------------------------------------------")
        print("Ticker: %s" % ticker)
        print("Average percent is: %f" % float(weekly_avg_returns * 100) + "%")
        # std_dev = np.std(returns, ddof=avg_returns)
        # print("Standard Deviation: %s" % std_dev)
        print("Standard Deviation is: %s" % weekly_stats_std_dev['std_dev'])
        print("Variance is: %s" % weekly_stats_std_dev['var'])
        print("--------------------------------------------------")

    newName = YahooTickerExch(ticker)
    name = newName.get_ticker_name()

    sec_info = ''
    try:
        series = Equity(ticker)
        sec_info = series.sector
    except TypeError:
        pass
    except KeyError:
        pass

    result = {'wk_avg_return': weekly_avg_returns,
              'wk_std_dev': round(weekly_stats_std_dev['std_dev'], 4),
              'day_avg_return': daily_avg_returns,
              'day_std_dev': round(daily_stats_std_dev['std_dev'], 4),
              'ticker': ticker,
              'sector': sec_info,
              'name': name}
    return result

if __name__ == '__main__':
    lines = []
    days = int()
    print("Provide the equity stock ticker(s) below to analyze:\n")
    while True:
        q1 = input()
        if q1:
            lines.append(q1)
        else:
            break
    print("How many historical days would you like to review?:\n")
    while True:
        q2 = input()
        if q2:
            days = int(q2)
            break
        else:
            print("No input supplied. Using default of 45 days...")
            days = 45
            break

    print("Analyzing past %i days..." % days)
    for stock in lines:
        ticker = stock
        # value = get_historic_data(ticker)
        # eq_shp = equity_sharpe(ticker)
        # mn_shp = market_neutral_sharpe(ticker, 'SPY')
        # weekly = weekly_std_dev(ticker, days=days, verbose=False, csv=False)
        # print(json.dumps(weekly))
        # daily = daily_std_dev(ticker, days=days, verbose=False, csv=False)
        # print(json.dumps(daily))
        try:
            comb = combined_day_week(ticker, days=days, verbose=False, csv=False)
            print(json.dumps(comb))
        except Exception:
            print("skipping ticker: %s" % ticker)
            continue

        # pprint(value)
        # pprint(eq_shp)
        # pprint(mn_shp)

