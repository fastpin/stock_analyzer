from rest_framework import serializers
from equities.lib.get_yahoo_quotes import YahooTicker
from equities.lib.ticker import weekly_std_dev, daily_std_dev, combined_day_week
from equities.models import EquitiesTracker
from pandas_finance import Equity
import logging

class StockTickerReportSerializer(serializers.Serializer):
    ticker_name = serializers.CharField(required=False)
    days = serializers.IntegerField(required=False, min_value=7, max_value=365)
    analysis = serializers.SerializerMethodField()

    def get_analysis(self, obj):
        logger = logging.getLogger('serializer')
        if 'days' not in obj.keys():
            try:
                db_days = EquitiesTracker.objects.get()
                n_days = db_days.default_days
            except EquitiesTracker.DoesNotExist:
                n_days = 60
            except Exception as e:
                raise Exception("Some internal issue happened setting default days: %s" % e.args)
        else:
            n_days = obj['days']
        if 'ticker_name' in obj.keys():
            try:
                # res = weekly_std_dev(obj['ticker_name'], days=n_days)
                res = combined_day_week(obj['ticker_name'], days=n_days)
                # print(res)
                # res_daily = daily_std_dev(obj['ticker_name'], days=n_days)
                res['wk_avg_return'] = res['wk_avg_return'] * 100
                res['day_avg_return'] = res['day_avg_return'] * 100

                try:
                    series = Equity(obj['ticker_name'])
                    res.update({'sector': series.sector})
                except TypeError:
                    # res.update({'sector': '<UNKNOWN>'})
                    pass
            except KeyError as e:
                raise serializers.ValidationError({"Error": "Invalid Ticker Provided"})
            # except Exception as e:
            #     res = {"Error": "Internal Server Error"}
            return res



class StockTickerListSerializer(serializers.ModelSerializer):
    # user = serializers.SlugRelatedField(slug_field='username', read_only=True)

    class Meta:
        model = EquitiesTracker
        fields = '__all__'