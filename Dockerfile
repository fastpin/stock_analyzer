FROM python:3.6
ENV PYTHONBUFFERED 1
RUN mkdir /config
ADD /config/requirements.pip /config/
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h
RUN pip install -r /config/requirements.pip
RUN mkdir /static;
RUN mkdir /src;
WORKDIR /src