# Stock Analyzer

This self-contained Django project can run as a docker container to analyze equity stock tickers' performance.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

If deploying to production - this project uses Docker, you will need to install docker-compose first.

```
sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

The Django database backend uses a local instance of PostGres, you will need to install/configure this with a database named `stock_analyzer`.

_(For MacOS users using HomeBrew package management)_
```bash
# To install
brew install postgresql

# To have launchd start postgresql now and restart at login:
brew services start postgresql

# Or, if you don't want/need a background service you can just run:
pg_ctl -D /usr/local/var/postgres start
```

Now create the necessary local DB:
```bash
createdb stock_analyzer
```

For more help with PostGres, visit https://www.codementor.io/engineerapart/getting-started-with-postgresql-on-mac-osx-are8jcopb

This project currently requires Python 3.5.1

### Installing

(If using _PyCharm_ as an IDE, you must set `stock_analyzer/src` as your sources root directory)

For development environment setup, you will need to clone the repository locally and perform the following.

Set Local environmental variable `DEV=1`

```bash
export DEV=1
```

#### SSL/TLS Certificates

You must locate the signed certificate and key in ```../certs``` relative to the root of this install.

Install necessary Python modules using the `pip` utility:
```bash
pip install -r stock_analyzer/config/requirements.pip
```
Next, to run a development server locally (using django-admin), you may pass the settings file path manually.
Refer to <https://docs.djangoproject.com/en/1.11/topics/settings/> for more info.

```bash
export DJANGO_SETTINGS_MODULE=stock_analyzer.settings.__init__
django-admin runserver --settings=stock_analyzer.settings.__init__
```

Once running you should be able to visit your brower URL and navigate the page


## Deployment

Once the code is pulled or cloned to the target deployment environment, compose the image locally
```bash
cd stock_analyzer/ && docker-compose build
```

Start the containers with:
```bash
cd stock_analyzer/ && docker-compose up -d
```

## Built With

* [Django Rest Framework](https://docs.djangoproject.com/en/1.11/) - The Web Framework Used
* [Django Rest Framework](http://www.django-rest-framework.org/) - The Django RestAPi web framework used
* [Bootstrap](http://getbootstrap.com/2.3.2/) - WebUI components
* [Mustache](https://mustache.github.io/) - Logic-less static templates
* [Docker](https://docs.docker.com/compose/) - Both Docker and Docker Compose)
* [Gunicorn](http://gunicorn.org/) - Python WSGI Webserver gateway
* [NGINX](https://www.nginx.com/) - Webserver used as reverse proxy
* [PostgreSQL](https://www.postgresql.org/) - Backend object-relational database


## Authors

* **Oliver Iotcovici** - *Complete Work* - [Fastpin](https://bitbucket.org/fastpin)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Much of the twitter bootstrap, mustache templating came from the webs
* Originally developed as a tool for a University UnderGrad Investing course
